package tech.quantit.northstar.strategy.api;

public interface ModuleNamingAware {

	void setModuleName(String name);
	
	String getModuleName();
}
